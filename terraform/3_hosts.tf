resource "openstack_compute_instance_v2" "k3s_master" {
  name            = "${var.PREFIX}_k3s_master"
  image_id        = var.HOST_IMAGE_ID
  flavor_name     = var.HOST_MASTER_FLAVOR
  key_pair        = var.SSH_KEY_PAIR
  security_groups = ["default"]

  network {
    name = "${var.PREFIX}_network"
    fixed_ip_v4 = "192.168.0.100"
  }
  
  depends_on = [
    openstack_networking_subnet_v2.subnet
  ]
}

resource "openstack_networking_floatingip_v2" "k3s_master_floating_ip" {
  pool = var.FLOATING_IP_POOL
}

resource "openstack_compute_floatingip_associate_v2" "k3s_master_floating_ip" {
  floating_ip = openstack_networking_floatingip_v2.k3s_master_floating_ip.address
  instance_id = openstack_compute_instance_v2.k3s_master.id

  provisioner "local-exec" {
    command = "echo \"export K3S_MASTER_IP=${openstack_networking_floatingip_v2.k3s_master_floating_ip.address}\" > hosts.env"
  }
}

resource "openstack_compute_instance_v2" "k3s_node_1" {
  name            = "${var.PREFIX}_k3s_node_1"
  image_id        = var.HOST_IMAGE_ID
  flavor_name     = var.HOST_NODE_FLAVOR
  key_pair        = var.SSH_KEY_PAIR
  security_groups = ["default"]

  network {
    name = "${var.PREFIX}_network"
    fixed_ip_v4 = "192.168.0.200"
  }

  depends_on = [
    openstack_networking_subnet_v2.subnet
  ]
}

resource "openstack_compute_instance_v2" "k3s_node_2" {
  name            = "${var.PREFIX}_k3s_node_2"
  image_id        = var.HOST_IMAGE_ID
  flavor_name     = var.HOST_NODE_FLAVOR
  key_pair        = var.SSH_KEY_PAIR
  security_groups = ["default"]

  network {
    name = "${var.PREFIX}_network"
    fixed_ip_v4 = "192.168.0.201"
  }

  depends_on = [
    openstack_networking_subnet_v2.subnet
  ]
}

resource "openstack_compute_instance_v2" "k3s_node_3" {
  name            = "${var.PREFIX}_k3s_node_3"
  image_id        = var.HOST_IMAGE_ID
  flavor_name     = var.HOST_NODE_FLAVOR
  key_pair        = var.SSH_KEY_PAIR
  security_groups = ["default"]

  network {
    name = "${var.PREFIX}_network"
    fixed_ip_v4 = "192.168.0.202"
  }

  depends_on = [
    openstack_networking_subnet_v2.subnet
  ]
}