variable "AUTH_URL" {}
variable "DOMAIN_NAME" {}
variable "USERNAME" {}
variable "PASSWORD" {}
variable "TENANT_ID" {}
variable "PREFIX" {}
variable "PUBLIC_NETWORK_ID" {}
variable "FLOATING_IP_POOL" {}
variable "SSH_KEY_PAIR" {}
variable "HOST_IMAGE_ID" {}
variable "HOST_MASTER_FLAVOR" {}
variable "HOST_NODE_FLAVOR" {}


terraform {
  required_providers {
    local = {
      source = "hashicorp/local"
    }
    openstack = {
      source = "terraform-provider-openstack/openstack"
    }
  }
  required_version = ">= 0.13"
}

provider "openstack" {
  domain_name = var.DOMAIN_NAME
  user_name   = var.USERNAME
  password    = var.PASSWORD
  tenant_id   = var.TENANT_ID
  auth_url    = var.AUTH_URL
}