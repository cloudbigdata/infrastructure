# Infrastructure
## Structure
The entire infrastructure is based on a single-master multi-node K3S cluster.
### Networks
All hosts are connected via one network (192.168.0.0/24). A router connects this network with the public network *ext-net-201* so that floating IPs can be assigned where necessary.

### Hosts
* 1 Master: Kubernetes controller (incl. database), no workloads
* 3 Nodes: Workloads (e.g. pods)

### Infrastructure services
Additional to the standard services provided by K3S (e.g. Traefik) we deploy these services to aid development and deployment:

* docker-registry: A private docker registry to push our generated images to

## Deployment
### Prerequisites
Create a file called *credentials.tfvars* in *terraform/* with the following content (replace examples with real values):
```shell
# Authentication
AUTH_URL="http://OPENSTACK_ADDRESS/v3"
DOMAIN_NAME="default"
USERNAME="USERNAME"
PASSWORD="PASSWORD"
TENANT_ID="TENANT_ID"
# Global settings
PREFIX="example" # Prefix used for all ressources
# Network
FLOATING_IP_POOL="ext-net-201"
PUBLIC_NETWORK_ID="3381f474-0676-4b6a-8ac9-1629c61e4701"
# Hosts
SSH_KEY_PAIR="NAME_OF_YOUR_SSH_KEY_PAIR"
HOST_IMAGE_ID="a0a1c616-f4f3-429d-8de9-8e74b5df805c"
HOST_MASTER_FLAVOR="m1.medium"
HOST_NODE_FLAVOR="m1.small"

```
PREFIX must be unique when deploying multiple instances of this infrastructure in the same OpenStack project.

Have the following applications installed:
* terraform
* ansible
* openssl
* kubectl

### Deploy
**Backup your local ~.kube/config, because this script will replace it!**

Run the following command:
```shell
./deploy.sh
```

This script will:

* Create networks and hosts on OpenStack
* Retrieve the public IP of the master node (use SSH jumps if you need to access the other nodes)
* Install the K3S cluster on the master and node hosts
* Replace you local *.kube/config* with one that has the information of the cluster
* Deploy a docker-registry on the cluster with a custom certificate that will be:
  * Copied to your local docker installation, so that you can push to the registry
  * Copied to all hosts of the cluster, so that Kubernetes can pull from the registry

### Destroy
Run the following command to remove all traces of the cluster from OpenStack:
```shell
./destroy.sh
```

### Troubleshooting
* **Ansible is stuck at "Wait for ssh":** Make sure that you can connect to the cluster via ssh (ubuntu@PUBLIC_IP). Check that there are no conflicts in your .ssh/known_hosts.
* **Ansible fails at kubectl commands:** Make sure that you can connect to the cluster. The command ```kubectl get nodes``` should return 4 nodes. If this is not the case run ```kubectl config view``` and check that the right config is used which points to your cluster. If this is not the case check that *~/.kube/config* contains the correct config.
  * Ubuntu: If *~/.kube/config* contains the correct config but it is not used by kubectl, create the file */var/snap/microk8s/current/args/kubectl* and put *--kubeconfig=/home/YOUR_USERNAME/.kube/config* in it. Now check that ```kubectl config view``` shows the correct config.

## Sources
* K3S cluster ansible roles: https://github.com/k3s-io/k3s-ansible
  * Everything regarding ARM, Raspbery Pis or Redhat Linux was removed
  
  
### Troubleshooting
* **my-hadoop-cluster-hadoop-yarn-nm-0 pending** 
* kubectl edit statefulsets.apps my-hadoop-cluster-hadoop-yarn-nm-0
* change container resources request from 2GiB to 500Mb 
* kubectl delete pod my-hadoop-cluster-hadoop-yarn-nm-0
* I've tried - WONT work kubectl patch statefulsets.apps my-hadoop-cluster-hadoop-yarn-nm -p '{
  "spec": {
    "template": {
      "containers": {
        "resources": {
          "requests": {
            "cpu": "1",
            "memory": "2Gi"
          }
        }
      }
    }
  }
}'


  
