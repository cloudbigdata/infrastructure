#!/bin/bash
set -e

cd terraform
if [ ! -d ".terraform" ]; then
  terraform init
fi
echo "Creating networks and hosts on OpenStack..."
terraform apply -var-file=credentials.tfvars

echo "Reading public IP of cluster..."
source hosts.env
echo "Public IP is $K3S_MASTER_IP"

echo "Creating k3s cluster..."
cd ../ansible
ansible-playbook k3s-cluster.yml -i inventory/cluster/hosts.ini

echo "Getting .kube/config from cluster..."
scp ubuntu@$K3S_MASTER_IP:~/.kube/config ~/.kube/config
sed -i "s/192.168.0.100/$K3S_MASTER_IP/g" ~/.kube/config

echo "Installing docker-registry on cluster..."
K3S_MASTER_IP=$K3S_MASTER_IP ansible-playbook docker-registry.yml -i inventory/cluster/hosts.ini

echo "Install docker-registry certificate locally..."
sudo mkdir -p /etc/docker/certs.d/$K3S_MASTER_IP:5000
sudo cp certs/registry.crt /etc/docker/certs.d/$K3S_MASTER_IP:5000/ca.crt

echo "Installing kafka and haddop on cluster..."
K3S_MASTER_IP=$K3S_MASTER_IP ansible-playbook kafka_hadoop.yml -i inventory/cluster/hosts.ini


echo "Done."
